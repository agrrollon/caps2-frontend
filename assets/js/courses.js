let adminUser = localStorage.getItem("isAdmin");
let activeuser = localStorage.getItem("isActive");

//cardfooter will be dynamically rendered if the use is an admin or not
let cardFooter;
//fetch
fetch("https://safe-atoll-56130.herokuapp.com/api/courses")
  .then((res) => res.json())
  .then((data) => {
    //log the data to check if you were able to fetch the data  from the  server
    console.log(data);

    let courseData;

    if (data.length < 1) {
      courseData = "No courses available.";
    } else {
      //else iterate the courses collection
      courseData = data.map((course) => {
          //to check the make up of each element inside the  courses collection
          console.log(course._id);

          //if hte user is regular user
          if (adminUser == "false" || !adminUser) {
            cardFooter = `
          <a href="./course.html?courseId=${course._id}" value="${course._id}" class="button btn-block">Select Course</a>
          

        `;
          } else {
            cardFooter = `  

            
          
            <a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="button btn-block">Edit Course</a>

            <a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="button btn-block">Delete Course</a>

            


        `;
          }
          return `
          <div class = "row">
            <div class =""col-md-6>
            <div class="course">
                <div class="preview">
                    <h6>Course</h6>
                    <h2>Web Development</h2>
                </div>
                <div class="info">
                    <h2>${course.name}</h2>
                    <p class="p-trunc">${course.description}</p>
                    <p>Price: ${course.price}</p>
                </div>
             <div class="card-footer">
                ${cardFooter}
            </div>
            </div>

            </div>
          </div>
          
          
        `;
        })
        .join("");
    }
    let container = document.querySelector("#coursesContainer");
    //get the value of courseData and assign it  as the #courseContainer's content
    container.innerHTML = courseData;
  });

//add modal button
let modalButton = document.querySelector("#adminButton");

if (adminUser == "false" || !adminUser) {
  modalButton.innerHTML = null;
} else {
  //display add course
  modalButton.innerHTML = `

    <div class="col-md-2 offset-md-10">
      <a href="./addCourse" class="button btn-block"" >Add Course</a>

    </div>  


  `;
  //when the admin clicks the add course button, it will redirect to the addCourse page
  let addCourseButton = document.querySelector("#adminButton");
  addCourseButton.addEventListener("click", (e) => {
    e.preventDefault();
    window.location.replace("./addCourse.html");
  });

}
let adminProfile = document.querySelector("#adminProfile")
let userProfile = document.querySelector("#userProfile")

if(adminUser == "false" || !adminUser) {

  userProfile.innerHTML = 
    `
      
        <a href="./userProfile.html" class="dropdown-item"> Profile </a>
               
    `
} else {
 
  adminProfile.innerHTML =
    `
      <li class="nav-item">
        <a href="./adminProfile.html" class="dropdown-item"> Profile </a>
      </li>
    `
}





