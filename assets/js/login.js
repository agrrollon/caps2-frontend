let loginForm = document.querySelector("#logInUser");
let adminUser = localStorage.getItem("isAdmin");

//add an event listener to the form
//(e) meaning when it is triggered, it will create an event object
loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	console.log(email);
	console.log(password);

	//add the logic that will generate an alert if the email and password are blank
	if(email == "" || password == "") {
			alert("please input your email and/or password")
	} else  {
			fetch('https://safe-atoll-56130.herokuapp.com/users/login', {
		        method: 'POST',
		        headers: {
		                'Content-Type': 'application/json'
		        },
		        body: JSON.stringify({
		            email: email,
		            password: password
		           })
		        })
		        .then(res => {
		            return res.json()
		        })
		        .then(data => {
		        	if(data.accessToken){
		        		console.log(data)
		        		

		        		localStorage.setItem('token', data.accessToken);
		        		fetch('https://safe-atoll-56130.herokuapp.com/api/users/details', {
		        			headers: {
		        				Authorization: `Bearer ${data.accessToken}`}
		        		})
		        		.then(res => {
		        			return res.json();
		        		})
		        		.then(data => {
		        			
		        			
		        			// localStorage.setItem("fullName", data.firstName + ' ' + data.lastName)
		        			localStorage.setItem("id", data._id);
		        			localStorage.setItem("isAdmin", data.isAdmin)
		        			window.location.replace("./courses.html")
		        			
		        		})
		        	} else {
		        		//authentication failure
		        		alert("Something went wrong!")
		        	}
		        })


		    }

 })
