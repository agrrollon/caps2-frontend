let token = localStorage.getItem('token');
let navbar = document.querySelector('#navbar');
let navItems = document.querySelector("#navSession")
let registerBtn = document.querySelector("#registerBtn")
let coursesBtn = document.querySelector('#coursesBtn');
let profileBtn = document.querySelector('#profileBtn');
let logOutBtn = document.querySelector('#registerBtn');
let isAdmin = localStorage.getItem('isAdmin');


if(!token) {

  navSession.innerHTML = 
  `
    <li class="nav-item"> 
      <a href="./pages/login.html" class="nav-link"> Log in </a>
    </li>
  `
  

} else {
  console.log('logged in')
  let profile = isAdmin === 'false' ? 'userProfile' : 'adminProfile';
  profileBtn.innerHTML = 
    `
      <li class="nav-item"> 
        <a href="./pages/${profile}.html" class="nav-link"> Profile </a>
      </li>
    `
  navSession.innerHTML = 
    `
      <li class="nav-item"> 
        <a href="./pages/logout.html" class="nav-link"> Logout </a>
      </li>
    ` 
}