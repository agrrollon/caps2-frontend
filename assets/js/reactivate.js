//this will reactivate a specific course

const activateCourse = () => {
		// getting id from url
	let token = localStorage.getItem('token');
	const url_string = window.location.href; //window.location.href
	const url = new URL(url_string);
	const courseId = url.searchParams.get("courseId");

	fetch(`https://safe-atoll-56130.herokuapp.com/api/courses/${courseId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//redirect to course
				alert('Course was successfully activated!')
				window.location.replace("./courses.html")
			} else {
				//redirect in creating course
				alert("Something went wrong!")
			}
		})

}

activateCourse();
