let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName")
let courseDescription = document.querySelector("#courseDescription")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

fetch(`https://safe-atoll-56130.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data)

	courseName.innerHTML = data.name
	courseDescription.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML =
	`
		<button id="enrollButton" class="button btn-block">Enroll</button>

	`

document.querySelector("#enrollButton").addEventListener("click", (e) => {
	e.preventDefault();
	fetch('https://safe-atoll-56130.herokuapp.com/api/users/enroll', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId : courseId
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		console.log(data)
		if (data === true) {
			//redirect to course page
			alert("Thank you for enrolling. See you!")
			window.location.replace('./courses.html')
		} else {
			alert("Something went wrong.")
		}
	})
})
})
//this will check if the user is admin or not
//if admin, it will redirect to the adminProfile
//if not, it will redirect to the userProfile
let adminProfile = document.querySelector("#adminProfile")
let userProfile = document.querySelector("#userProfile")

if(adminUser == "false" || !adminUser) {
  //if user is regular user, do not show add course button
  userProfile.innerHTML = 
    `
      
        <a href="./userProfile.html" class="dropdown-item"> Profile </a>
               
    `
} else {
  //display add course if user is an admin
  adminProfile.innerHTML =
    `
      <li class="nav-item">
        <a href="./adminProfile.html" class="dropdown-item"> Profile </a>
      </li>
    `
}
